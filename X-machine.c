#include <stdio.h>
#include <stdlib.h>

#define SUB 1
#define CON 2
#define PC t[0]

int * t;
unsigned int ts;

int readfile(char * filename)
{
    FILE * infile = fopen(filename, "r");
    if (infile == NULL) return 1;

    int tmp;
    t = (int *) malloc(sizeof(int));
    ts = 0;
    unsigned int tsize = 1;
    while (fscanf(infile, "%d", &tmp) == 1)
    {
        ts++;
        if (ts > tsize)
        {
            tsize <<= 1;
            t = realloc(t, tsize * sizeof(int));
        }
        if (t == NULL) return 1;
        t[ts - 1] = tmp;
    }

    int result = fclose(infile);
    if (result != 0) return 1;
    return 0;
}

int main(int argc, char ** argv)
{
    if (argc != 2)
    {
        printf("usage: X-machine filename\n");
    }
    else
    {
        // read file
        char * filename = argv[1];
        int result = readfile(filename);
        if (result != 0)
        {
            printf("could not read file");
            exit(result);
        }

        // start machine
        while (1)
        {
            if (t[PC] == SUB)
            {
                t[t[PC + 3]] = t[t[PC + 1]] - t[t[PC + 2]];
                PC += 4;
            }
            else if (t[PC] == CON)
            {
                if (t[t[PC + 1]] < t[t[PC + 2]])
                {
                    t[t[PC + 3]] = PC;
                }
                else
                {
                    PC += 4;
                }
            }
            else
            {
                break;
            }
        }

        // print tape
        for (unsigned int i = 0; i < ts; i++) {
            printf("%i", t[i]);
            if (i != ts - 1)
            {
                printf(" ");
            }
            else
            {
                printf("\n");
            }
        }
    }
    return 0;
}
