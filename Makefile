CC=clang
CFLAGS=-c -O3 -std=c99 -Wall -Wconversion -Wextra
LDFLAGS=

all: X-machine.c X-machine

X-machine: X-machine.o
	$(CC) $(LDFLAGS) X-machine.o -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f X-machine X-machine.o
